# El-MAVEN

## Steps

1. Install metalsmith

   ```bash
   $ npm install metalsmith
   $ npm install metalsmith-in-place
   $ npm install metalsmith-layouts
   $ npm install nunjucks
   $ npm install jstransformer-nunjucks
   $ npm install metalsmith-markdown
   $ npm install metalsmith-debug --save
   $ npm install eslint eslint-config-prettier
   ```

2. Build `html` files

   ```bash
   $ node build.js
   ```

## Directory Structure

```
├── src - nunjucks for fixed material
|   ├── layouts - nested templates
|   ├── assets - all fixed content
|   ├── pages -
        ├── index.njk
        ├── downloads.njk

├── content - all variable content
├── build - contains generated html files
├── docs - documentation
├── node_modules - contains npm packages
├── package-lock.json - npm install history tree
├── .eslintrc.js
├── .gitignore.js
├── README.md
├── LICENSE.txt

```

## Resources

1. https://www.glinka.co/blog/metalsmith-layouts-nunjucks/
2. https://mozilla.github.io/nunjucks/templating.html
