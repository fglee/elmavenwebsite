const toUpper = function (string) {
    "use strict";
    return string.toUpperCase();
};

const spaceToDash = function (string) {
    "use strict";
    return string.replace(/\s+/g, "-");
};

const inplace = require('metalsmith-in-place');
const assets = require('metalsmith-assets-improved');
const layouts = require('metalsmith-layouts');
const metalsmith = require('metalsmith');


metalsmith(__dirname)
    .metadata({
            site: {
                url: 'https://www.github.com/elucidatainc/elmaven',
                title: 'El-MAVEN'
            }
            .source('./src/pages')
            .destination('./build/')
            .clean(true)
            .use(inplace(templateConfig))
            .use(layouts(templateConfig))
            .use(assets({
                src: 'src/assets'
            }))
            .clean(true)
            .build(function (err) {
                if (err) throw err;
                console.log('Build finished!');
            });